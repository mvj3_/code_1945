//1. 在assert目录下新建xml文件student.xml
<?xml version="1.0" encoding="UTF-8"?>
<students>
    <student>
        <id>2163</id>
        <name>Kumar</name>
        <age>16</age>
        <sex>male</sex>
        <address>Fuyong street, Shenzhen Guangdong China</address>
    </student>
    <student>
        <id>6752</id>
        <name>Siva</name>
        <age>18</age>
        <sex>female</sex>
        <address>Xixiang street, Shenzhen Guangdong China</address>
    </student>
    <student>
        <id>6763</id>
        <name>Timmy</name>
        <age>22</age>
        <sex>male</sex>
        <address>Longgang street, Shenzhen Guangdong China</address>
    </student>
</students>

//2. student信息类
public class Student {
	
	private int id;
	private String name;
	private int age;
	private String sex;
	private String address;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "Student info[ id=" + id + ", name=" + name
				+ ", age=" + age + ", sex=" + sex + ", address=" + address + "]";
	}

}

//3.解析
/**
 * 1.SAX解析器的工作方式是自动将事件推入注册的事件处理器进行处理，因此你不能控制事件的处理主动结束；
 * 每次都会解析整个XML文档，浪费了处理器资源和延长了处理的时间。
 * 而Pull解析器的工作方式为允许你的应用程序代码主动从解析器中获取事件，正因为是主动获取事件，
 * 因此可以在满足了需要的条件后不再获取事件，结束解析。
 * 2.而他们的相似性在运行方式上，Pull解析器也提供了类似SAX的事件（开始文档START_DOCUMENT和结束文档END_DOCUMENT，
 * 开始元素START_TAG和结束元素END_TAG，遇到元素内容TEXT等），但需要调用next() 方法提取它们（主动提取事件）。
 * */

public class XMLPullParserHandler {
	
	private List<Student> students;
	private Student student;
	private String text;
	
	//private boolean isDone = false; //设置退出解析的标志
	
	public XMLPullParserHandler() {
		students = new ArrayList<Student>();
	}
	
	public List<Student> getEmployees() {
		return students;
	}
	
	public List<Student> parser(InputStream is) {
		XmlPullParserFactory factory = null;
		XmlPullParser parser = null;
		try {
			factory = XmlPullParserFactory.newInstance();//实例化pull解析工厂类对象
			factory.setNamespaceAware(true);//明确下面的parser是由该工厂生产的
			parser = factory.newPullParser();//新建pull解析器
			parser.setInput(is, null);//加入解析的对象
			int eventType = parser.getEventType();//获取事件类型
			while (eventType != XmlPullParser.END_DOCUMENT/* && !isDone */) {
				String tagname = parser.getName();//获取节点名称
				switch (eventType) {
				case XmlPullParser.START_TAG :
					if (tagname.equals("student")) {
						student = new Student();
					}
					break;
				case XmlPullParser.TEXT ://某个节点下面的内容
					text = parser.getText();
					break;
				case XmlPullParser.END_TAG :
					if (tagname.equalsIgnoreCase("student")) {
						//add student object to list
						students.add(student);
					} else if (tagname.equalsIgnoreCase("name")){
						student.setName(text);
					} else if (tagname.equalsIgnoreCase("id")) {
						student.setId(Integer.parseInt(text));
					} else if (tagname.equalsIgnoreCase("age")) {
						student.setAge(Integer.parseInt(text));
					} else if (tagname.equalsIgnoreCase("sex")) {
						student.setSex(text);
					} else if (tagname.equalsIgnoreCase("address")) {
						student.setAddress(text);
					}
					
					//如果后面的内容不需要解析了可以设置标记退出解析
					//isDone = true;
					break;
				default : 
					break;
				}
				eventType = parser.next();//继续检索下一个节点事件
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return students;
	}
}

//4. 将解析结果加载到listview中
public class MainActivity extends Activity {
	
	private ListView listview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listview = (ListView) findViewById(R.id.list);
		List<Student> students = null;
		try {
			XMLPullParserHandler handler = new XMLPullParserHandler();
			students = handler.parser(getAssets().open("student.xml"));
			ArrayAdapter<Student> adapter = new ArrayAdapter<Student>(this, 
					R.layout.list_item,
					students);
			listview.setAdapter(adapter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}


